
const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const route = require('./server/route/index');

mongoose.connect('mongodb://localhost:27017/nodeJsTask'); // creating connection with mongoDB database.

const app = express();

// parse the req params and atteche them to req.body.

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

const router = express.Router();

const port = 3000;

app.use(route);

app.listen(port);

console.log('server running on port '+port);