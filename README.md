# Steps to execute

## Clone the repository

```
git clone https://skartikey21@bitbucket.org/skartikey21/nodejstask.git
```

## Install dependency

```
npm install
```

## Start main file

```
node app.js
```