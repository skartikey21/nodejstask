const ProductMaster = require('../models/ProductMaster.model'); // Importing Product Schema.

// Create Product master.

exports.AddProductMaster = [
    (req, res) => {
        ProductMaster.CreateProductMaster(req.body).then((result) => {
            if (!result) {
                res.status(422).send({ error: 'data not created' });
            } else {
                res.status(200).send(result);
            }
        }).catch((err) => {
            res.status(500).send(err);
        })
    },
];

// Read Product master with pagination.

exports.GetProductWithPagination = [
    (req, res) => {
        var previous = req.query.previous;
        var next = req.query.next;
        var limit = 10;
        var fields = {};
        var populate = [{ path: 'CategoryId' }]
        ProductMaster.GetProductListWithPagination(previous, next, limit, fields, populate)
        .then((result) => {
            if (!result) {
                res.status(404).send('data not found');
            } else {
                res.status(200).send(result);
            }
        }).catch((err) => {
            res.status(500).send(err);
        });
    },
];

// Read Product master by id.

exports.ReadProductMasterById = [
    (req, res) => {
        ProductMaster.model.find({ _id: req.query._id })
            .then((result) => {
                if (!result) {
                    res.status(404).send({ error: 'data not found' });
                } else {
                    res.status(200).send(result);
                }
            }).catch((err) => {
                res.status(500).send(err);
            });
    },
];

// Update Product master by id.

exports.UpdateProductMaster = [
    (req, res) => {
        ProductMaster.UpdateProductMaster(req.params._id, req.body)
            .then((result) => {
                if (!result) {
                    res.status(404).send({ error: 'data not found' });
                } else {
                    res.status(200).send(result);
                }
            }).catch((err) => {
                res.status(500).send(err);
            });
    },
];

// Remove Product master by id.

exports.RemoveProductMaster = [
    (req, res) => {
        ProductMaster.DeleteProductMaster(req.params._id)
            .then((result) => {
                if (!result) {
                    res.status(404).send({ error: 'data not found' });
                } else {
                    res.status(200).send({ message: 'Record Deleted' });
                }
            }).catch((err) => {
                res.status(500).send(err);
            });
    },
];