const CategoryMaster = require('../models/CategoryMaster.model'); // importing category schema.

// Create Category master.

exports.AddCategoryMaster = [
    (req, res) => {
        CategoryMaster.CreateCategoryMaster(req.body).then((result) => {
            if (!result) {
                res.status(422).send({ error: 'data not created' });
            } else {
                res.status(200).send(result);
            }
        }).catch((err) => {
            res.status(500).send(err);
        })
    },
];

// Read Category master with pagination.

exports.ReadCategoryMaster = [
    (req, res) => {
        CategoryMaster.GetCategoryMaster().then((result) => {
            if (!result) {
                res.status(404).send({ error: 'data not found' });
            } else {
                res.status(200).send(result);
            }
        }).catch((err) => {
            res.status(500).send(err);
        })
    },
];

// Read Category master by id.

exports.ReadCategoryMasterById = [
    (req, res) => {
        CategoryMaster.GetCategoryMasterById({ _id: req.params._id })
            .then((result) => {
                if (!result) {
                    res.status(404).send({ error: 'data not found' });
                } else {
                    res.status(200).send(result);
                }
            }).catch((err) => {
                res.status(500).send(err);
            });
    },
];

// Update Category master by id.

exports.UpdateCategoryMaster = [
    (req, res) => {
        CategoryMaster.UpdateCategoryMaster(req.params._id, req.body)
            .then((result) => {
                if (!result) {
                    res.status(404).send({ error: 'data not found' });
                } else {
                    res.status(200).send(result);
                }
            }).catch((err) => {
                res.status(500).send(err);
            });
    },
];

// Remove Category master by id.

exports.RemoveCategoryMaster = [
    (req, res) => {
        CategoryMaster.DeleteCategoryMaster(req.params._id)
            .then((result) => {
                if (!result) {
                    res.status(404).send({ error: 'data not found' });
                } else {
                    res.status(200).send({ message: 'record deleted'});
                }
            }).catch((err) => {
                res.status(500).send(err);
            });
    },
];