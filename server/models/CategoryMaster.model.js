const mongoose = require('mongoose');

/**
 *  CategoryMaster Schema
 */

const CategoryMasterSchema = new mongoose.Schema({
  CategoryName: {
    type: String,
  },
});

const CategoryMaster = mongoose.model('Category', CategoryMasterSchema);

exports.CreateCategoryMaster = data => CategoryMaster.create(data); // function to create Category.

exports.GetCategoryMaster = () => CategoryMaster.find(); // function to read Category.

exports.GetCategoryMasterById = _id => CategoryMaster.findById(_id); // function to read Category by id.

exports.UpdateCategoryMaster = (_id, data) => CategoryMaster.findByIdAndUpdate({ _id }
  , { $set: data }, {  new: true }); // function to update Category by id.

exports.DeleteCategoryMaster = _id => CategoryMaster.findByIdAndRemove({ _id }); // remove Category by id.

// Export the Mongoose model

exports.model = CategoryMaster;