const mongoose = require('mongoose');
const paginate = require('cursor_paginate');

/**
 *  ProductMaster Schema
 */

const ProductMasterSchema = new mongoose.Schema({
  ProductName: {
    type: String,
  },
  CategoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
  }
});

ProductMasterSchema.plugin(paginate);

const ProductMaster = mongoose.model('Product', ProductMasterSchema);

exports.CreateProductMaster = data => ProductMaster.create(data);  // function to create product.

exports.GetProductListWithPagination = (previous, next, limit, fields, populate) => 
ProductMaster.paginate(previous,
  next,
  limit,
  fields,
  populate);  // function to read product with pagination.

exports.GetProductMasterById = _id => ProductMaster.findById(_id); // function to read product by id.

exports.UpdateProductMaster = (_id, data) => ProductMaster.findByIdAndUpdate({ _id }
  , { $set: data }, { new: true }); // function to update product by id.

exports.DeleteProductMaster = _id => ProductMaster.findByIdAndRemove({ _id }); // remove product by id.

// Export the Mongoose model

exports.model = ProductMaster;



