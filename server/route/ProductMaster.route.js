const ProductMaster = require('../controllers/ProductMaster.controller'); // importing product controller.

const express = require('express');

const route = express.Router();

route.post('/',ProductMaster.AddProductMaster);  // api to create product.

// route.get('/',ProductMaster.GetProductWithPagination); // api to read product.

route.get('/',ProductMaster.ReadProductMasterById); // api to read product by id.

route.put('/:_id',ProductMaster.UpdateProductMaster); // api to update product by id.

route.delete('/:_id',ProductMaster.RemoveProductMaster); // api to remove product by id.

module.exports = route;