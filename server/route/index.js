const express = require('express');

const CategoryMaster = require('../route/CategoryMaster.route'); // importing category route.

const ProductMaster = require('../route/ProductMaster.route'); // importing category route.

/**
 * Defining express route.  
 */

const route = express.Router();

route.use('/category', CategoryMaster);

route.use('/product', ProductMaster);

module.exports=route;

