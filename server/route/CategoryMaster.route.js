const CategoryMaster = require('../controllers/CategoryMaster.controller'); // importing category controller.

const express=require('express');

const route =express.Router();

route.post('/',CategoryMaster.AddCategoryMaster); // api to create category.

route.get('/',CategoryMaster.ReadCategoryMaster); // api to read category.

route.get('/:_id',CategoryMaster.ReadCategoryMasterById); // api to read category by id.

route.put('/:_id',CategoryMaster.UpdateCategoryMaster); // api to update category by id.

route.delete('/:_id',CategoryMaster.RemoveCategoryMaster); // api to remove category by id.

module.exports= route;